<?php

/**
 * Enqueue scripts and styles.
 */
function ecommercestore_scripts() {

	wp_enqueue_style( 'ecommercestore-core-style', get_template_directory_uri().'/assets/css/core.css', array(), _S_VERSION );
	wp_enqueue_style( 'ecommercestore-theme-style', get_template_directory_uri().'/assets/css/theme-beige.css', array(), _S_VERSION );
	wp_enqueue_style( 'ecommercestore-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'ecommercestore-style', 'rtl', 'replace' );	


	wp_enqueue_script( 'tether', get_template_directory_uri() . '/js/tether.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20151215', true );
	wp_enqueue_script( 'wc-add-to-cart-variation' );
	wp_enqueue_script( 'moment', get_template_directory_uri() . '/js/moment.js', array(), '20151215', true );
	wp_enqueue_script( 'bootstrap-datepicker', get_template_directory_uri() . '/js/bootstrap-datepicker.js', array(), '20151215', true );
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array(), '20151215', true );
	wp_enqueue_script( 'appear', get_template_directory_uri() . '/js/jquery.appear.js', array(), '20151215', true );
	wp_enqueue_script( 'scrollTo', get_template_directory_uri() . '/js/jquery.scrollTo.min.js', array(), '20151215', true );
	wp_enqueue_script( 'localscroll', get_template_directory_uri() . '/js/jquery.localScroll.min.js', array(), '20151215', true );
	wp_enqueue_script( 'validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array(), '20151215', true );
	wp_enqueue_script( 'YTPlayer', get_template_directory_uri() . '/js/jquery.mb.YTPlayer.min.js', array(), '20151215', true );
	wp_enqueue_script( 'twitterFetcher_min', get_template_directory_uri() . '/js/twitterFetcher_min.js', array(), '20151215', true );
	wp_enqueue_script( 'skrollr', get_template_directory_uri() . '/js/skrollr.min.js', array(), '20151215', true );
	wp_enqueue_script( 'animsition', get_template_directory_uri() . '/js/animsition.min.js', array(), '20151215', true );
	wp_enqueue_script( 'soup-core', get_template_directory_uri() . '/js/core.js', array(), '20151215', true ); 
    //wp_enqueue_script( 'map-api', 'https://maps.googleapis.com/maps/api/js?key='.$soup_map_api, array(), '', false ); 
	wp_enqueue_script( 'soup-custom', get_template_directory_uri() . '/js/custom.js', array(), '20151215', true );
	wp_enqueue_script( 'soup-check', get_template_directory_uri() . '/js/check.js', array(), '20151215', true );
	wp_enqueue_script( 'soup-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'soup-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );


	// Localize the script with new data
	$translation_array = array(
		'ajaxurl' => admin_url('admin-ajax.php'),
		'soup_uri' => get_template_directory_uri(), 
		'soup_home' => home_url('/'), 
		'soup_mpico' => $soup_map_icon ,
		'soup_mplat' => $soup_map_lat ,
		'soup_mplong' => $soup_map_long,
		'soup_slidespeed' => (!empty($soup['slider-speed'])) ? $soup['slider-speed'] : 3500,
		'hoursDisabled' => $soup['disabled_hour'] ? $soup['disabled_hour'] : '',
		'datesDisabled' => $soup['disabled_date'] ? $soup['disabled_date'] : ''
	); 
	if(class_exists('WooCommerce')){
		$translation_array['singleProduct'] = is_product();
	}
	wp_localize_script( 'soup-core', 'object_name', $translation_array );
	wp_localize_script( 'soup-custom', 'object_name', $translation_array );
	wp_localize_script( 'soup-check', 'object_name', $translation_array );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ecommercestore_scripts' );