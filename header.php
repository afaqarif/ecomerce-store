<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package EcommerceStore
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
	<!-- Body Wrapper -->
	<div id="body-wrapper" class="animsition">

	    <!-- Header -->
	    <header id="header" class="light">

	        <div class="container">
	            <div class="row">
	                <div class="col-md-3">
	                    <!-- Logo -->
	                    <div class="module module-logo dark">
	                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
	                            <img src="<?php the_field('desktop_logo', 'option'); ?>" alt="" width="88">
	                        </a>
	                    </div>
	                </div>
	                <div class="col-md-7">
	                    <!-- Navigation -->
	                    <nav class="module module-navigation left mr-4">
							<?php
								wp_nav_menu(
									array(
										'menu_id'        => 'nav-main',
										'menu_class'	=>	'nav nav-main',
										'container'		=> 'ul',

									)
								);
							?>


	                        <ul id="nav-main" class="nav nav-main">
	                            <li>
	                                <a href="#">Home</a>
	                            </li>
	                            <li>
	                                <a href="#">About</a>
	                            </li>
	                            <li>
	                                <a href="#">Menu</a>
	                            </li>
	                            <li><a href="page-contact.html">Contact</a></li>
	                        </ul>
	                    </nav>
	                    <div class="module left">
	                        <a href="menu-list-navigation.html" class="btn btn-outline-secondary"><span>Order</span></a>
	                    </div>
	                </div>
	                <div class="col-md-2">
	                    <a href="#" class="module module-cart right" data-toggle="panel-cart">
	                        <span class="cart-icon">
	                            <i class="ti ti-shopping-cart"></i>
	                            <span class="notification">0</span>
	                        </span>
	                        <span class="cart-value">$<span class="value">0.00</span></span>
	                    </a>
	                </div>
	            </div>
	        </div>

	    </header>
	    <!-- Header / End -->

	    <!-- Header -->
	    <header id="header-mobile" class="light">

	        <div class="module module-nav-toggle">
	            <a href="#" id="nav-toggle" data-toggle="panel-mobile"><span></span><span></span><span></span><span></span></a>
	        </div>

	        <div class="module module-logo">

	            <a href="index.html">
	                <img src="assets/img/logo-horizontal-dark.svg" alt="">

	            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
	                <img src="<?php the_field('mobile_logo', 'option'); ?>" alt="">

	            </a>
	        </div>

	        <a href="#" class="module module-cart" data-toggle="panel-cart">
	            <i class="ti ti-shopping-cart"></i>
	            <span class="notification">0</span>
	        </a>

	    </header>

	    <!-- Header / End -->

	    <div id="content">

