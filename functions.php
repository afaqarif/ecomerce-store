<?php
/**
 * EcommerceStore functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package EcommerceStore
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'ecommercestore_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ecommercestore_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on EcommerceStore, use a find and replace
		 * to change 'ecommercestore' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'ecommercestore', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'ecommercestore' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'ecommercestore_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

			add_theme_support( 'woocommerce', array(
        'thumbnail_image_width' => 150,
        'single_image_width'    => 300,

        'product_grid'          => array(
            'default_rows'    => 3,
            'min_rows'        => 2,
            'max_rows'        => 8,
            'default_columns' => 4,
            'min_columns'     => 2,
            'max_columns'     => 5,
        ),
      ));
	}
endif;
add_action( 'after_setup_theme', 'ecommercestore_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ecommercestore_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'ecommercestore_content_width', 640 );
}
add_action( 'after_setup_theme', 'ecommercestore_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ecommercestore_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'ecommercestore' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'ecommercestore' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'ecommercestore_widgets_init' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Wp Enqueue File 
 */
require get_template_directory() . '/inc/wp_enqueue.php';

require get_template_directory() . '/vc_extend/extend_vc.php';

//require get_template_directory() . '/vc_templates/vc_row.php';


/**
 * Theme options Page
 */

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

function theme_ajax_url(){
    echo '<script type="text/javascript">
    var ajaxurl = "' . admin_url('admin-ajax.php') . '";
    var siteurl = "' .site_url(). '";
    </script>';
}
add_action('wp_head', 'theme_ajax_url');

 // display lowest price from variation product
function soup_variation_price_format( ) {
	global $product;
	$wo_price = wc_price($product->get_price());
	if( $product->is_type( 'variable' ) ){
		$min_price = $product->get_variation_price( 'min', true );
		$max_price = $product->get_variation_price( 'max', true );
		if ( $min_price != $max_price ) {
			$price = sprintf( wp_kses( '<span class="text-muted">from</span> %1$s', 'soup' ), wc_price( $min_price ) );
			return $price;
		} else {
			$price = sprintf( wp_kses( '%1$s', 'soup' ), wc_price( $min_price ) );
			return $price;
		}
	}else{
		return $wo_price;
	}
}

//product quecik view data

function soup_product_view(){
    ob_start();
    $return_str = ''; 
   
    $productId =  intval($_POST['productId']);  
    global $product;
    $_product = wc_get_product( $productId );

 
    // Check if the input was a valid integer
    if ( $productId == 0 ) {
        esc_html_e('Invalid Input','soup'); 
    }else{
        $args = array(
            'post_type'=>'product',
            'posts_per_page'=>1,
            'p'=> $productId
        );
        $the_query = new WP_Query( $args ); 
        if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 
			    <div class="modal-content">
			        <div class="modal-header modal-header-lg dark bg-dark">
			            <?php global $soup,$post; 
			                $popup_title = (!empty($soup['pop_up_title'])) ? $soup['pop_up_title'] : esc_html__('Specify your dish','soup'); 
                            $pop = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
                            if($soup['pop_img']!=1){
                                $popup_title_bgimh = $pop[0];
                            }else{
                                $popup_title_bgimh = (!empty($soup['pop_up_bgimg']['url'])) ? $soup['pop_up_bgimg']['url'] : get_template_directory_uri().'/assets/img/photos/modal-add.jpg';
                            }   
			            ?>
                        <div class="bg-image" style="background-image: url(<?php echo esc_url($popup_title_bgimh); ?>);"><img src="<?php echo esc_url($popup_title_bgimh); ?>" alt="<?php esc_attr_e('soup wordpress theme','soup'); ?>"></div>
			            <h4 class="modal-title"><?php echo esc_html($popup_title); ?></h4>
			            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
			        </div>
			        <div class="modal-product-details">
			            <div class="row align-items-center">
			                <div class="col-8">
			                    <h6 class="mb-0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
			                    <span class="text-muted"><?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ); ?></span>
			                </div>
			                <div class="col-4 text-lg text-right"><?php echo soup_variation_price_format(); ?></div>
			            </div>
			        </div>
			        <div class="modal-body panel-details-container popsoup">  
			            <?php do_action( 'woocommerce_single_product_cart' ); ?>
			        </div> 
			    </div> 
            <?php endwhile;
        wp_reset_postdata();
        else :
           esc_html_e('No posts found','soup'); 
        endif;
    } 
    ob_flush();
    die; 
}
// creating Ajax call for WordPress
add_action( 'wp_ajax_nopriv_soup_product_view', 'soup_product_view' );
add_action( 'wp_ajax_soup_product_view', 'soup_product_view' );


add_action( 'woocommerce_single_product_cart', 'woocommerce_template_single_add_to_cart', 30 );
 
// START ajax add to cart By Sajib Talukder
add_action( 'wp_ajax_nopriv_variation_to_cart', 'product_variation_add_to_cart' );
add_action( 'wp_ajax_variation_to_cart', 'product_variation_add_to_cart' );
function product_variation_add_to_cart(){

    $product_id = (isset($_POST['pid']) && !empty($_POST['pid'])) ? $_POST['pid'] : get_the_ID(); 
    $prodct_quantity = (isset($_POST['quantity']) && !empty($_POST['quantity'])) ? $_POST['quantity'] : 1 ;
    $item = (!empty($_POST['item'])) ? $_POST['item'] : 0 ; 
    if($item!=0){
        $var_id = find_matching_product_variation_id( $product_id, $item );
        $var_id = (!empty($var_id)) ? $var_id : 0;
        WC()->cart->add_to_cart( $product_id, $prodct_quantity, $var_id ); 
    }else{
        WC()->cart->add_to_cart( $product_id, $prodct_quantity,0 ); 
    }   

    // $count = WC()->cart->get_cart_contents_count();
    WC_AJAX::get_refreshed_fragments();

    // die(); // To avoid server error 500
}

function find_matching_product_variation_id($product_id, $attributes){
    return (new WC_Product_Data_Store_CPT())->find_matching_product_variation(
        new WC_Product($product_id),
        $attributes
    );
}

// END ajax add to cart By Sajib Talukder
 

// Remove product in the cart using ajax
function warp_ajax_product_remove()
{ 
   $product_id = $_POST['product_id']; 
   $cart_item_key = $_POST['cart_item_key']; 
   WC()->cart->remove_cart_item($cart_item_key);
   WC_AJAX::get_refreshed_fragments();
    
}

add_action( 'wp_ajax_product_remove', 'warp_ajax_product_remove' );
add_action( 'wp_ajax_nopriv_product_remove', 'warp_ajax_product_remove' );

